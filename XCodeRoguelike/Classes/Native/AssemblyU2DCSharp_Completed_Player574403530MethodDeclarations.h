﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Completed.Player
struct Player_t574403530;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"

// System.Void Completed.Player::.ctor()
extern "C"  void Player__ctor_m2611490551 (Player_t574403530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.Player::Start()
extern "C"  void Player_Start_m1558628343 (Player_t574403530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.Player::OnDisable()
extern "C"  void Player_OnDisable_m729886686 (Player_t574403530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.Player::Update()
extern "C"  void Player_Update_m1078690550 (Player_t574403530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.Player::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void Player_OnTriggerEnter2D_m2789272417 (Player_t574403530 * __this, Collider2D_t1890038195 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.Player::Restart()
extern "C"  void Player_Restart_m3465508196 (Player_t574403530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.Player::LoseFood(System.Int32)
extern "C"  void Player_LoseFood_m3579051793 (Player_t574403530 * __this, int32_t ___loss, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.Player::CheckIfGameOver()
extern "C"  void Player_CheckIfGameOver_m2655384480 (Player_t574403530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
