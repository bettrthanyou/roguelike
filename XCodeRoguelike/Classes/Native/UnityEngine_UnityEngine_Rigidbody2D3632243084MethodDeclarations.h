﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Rigidbody2D
struct Rigidbody2D_t3632243084;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "UnityEngine_UnityEngine_Rigidbody2D3632243084.h"

// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_position()
extern "C"  Vector2_t3525329788  Rigidbody2D_get_position_m3766784193 (Rigidbody2D_t3632243084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_get_position(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_get_position_m80650910 (Rigidbody2D_t3632243084 * __this, Vector2_t3525329788 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::MovePosition(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_MovePosition_m816157558 (Rigidbody2D_t3632243084 * __this, Vector2_t3525329788  ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_CALL_MovePosition_m3017526385 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t3632243084 * ___self, Vector2_t3525329788 * ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
