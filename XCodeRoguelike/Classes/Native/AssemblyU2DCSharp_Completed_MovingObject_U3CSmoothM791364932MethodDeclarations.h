﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Completed.MovingObject/<SmoothMovement>c__Iterator0
struct U3CSmoothMovementU3Ec__Iterator0_t791364932;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Completed.MovingObject/<SmoothMovement>c__Iterator0::.ctor()
extern "C"  void U3CSmoothMovementU3Ec__Iterator0__ctor_m3383116808 (U3CSmoothMovementU3Ec__Iterator0_t791364932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Completed.MovingObject/<SmoothMovement>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSmoothMovementU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m275464394 (U3CSmoothMovementU3Ec__Iterator0_t791364932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Completed.MovingObject/<SmoothMovement>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSmoothMovementU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3594262622 (U3CSmoothMovementU3Ec__Iterator0_t791364932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Completed.MovingObject/<SmoothMovement>c__Iterator0::MoveNext()
extern "C"  bool U3CSmoothMovementU3Ec__Iterator0_MoveNext_m141991660 (U3CSmoothMovementU3Ec__Iterator0_t791364932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.MovingObject/<SmoothMovement>c__Iterator0::Dispose()
extern "C"  void U3CSmoothMovementU3Ec__Iterator0_Dispose_m4078259781 (U3CSmoothMovementU3Ec__Iterator0_t791364932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.MovingObject/<SmoothMovement>c__Iterator0::Reset()
extern "C"  void U3CSmoothMovementU3Ec__Iterator0_Reset_m1029549749 (U3CSmoothMovementU3Ec__Iterator0_t791364932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
