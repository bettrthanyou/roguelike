﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// Completed.Enemy
struct Enemy_t2543389745;

#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_Completed_Enemy2543389745.h"

#pragma once
// Completed.Enemy[]
struct EnemyU5BU5D_t3447615468  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Enemy_t2543389745 * m_Items[1];

public:
	inline Enemy_t2543389745 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Enemy_t2543389745 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Enemy_t2543389745 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
