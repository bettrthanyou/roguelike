﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Completed.MovingObject
struct MovingObject_t1424344598;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4082783401.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void Completed.MovingObject::.ctor()
extern "C"  void MovingObject__ctor_m184393323 (MovingObject_t1424344598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.MovingObject::Start()
extern "C"  void MovingObject_Start_m3426498411 (MovingObject_t1424344598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Completed.MovingObject::Move(System.Int32,System.Int32,UnityEngine.RaycastHit2D&)
extern "C"  bool MovingObject_Move_m118298465 (MovingObject_t1424344598 * __this, int32_t ___xDir, int32_t ___yDir, RaycastHit2D_t4082783401 * ___hit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Completed.MovingObject::SmoothMovement(UnityEngine.Vector3)
extern "C"  Il2CppObject * MovingObject_SmoothMovement_m1746373659 (MovingObject_t1424344598 * __this, Vector3_t3525329789  ___end, const MethodInfo* method) IL2CPP_METHOD_ATTR;
