﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Completed.BoardManager/Count
struct Count_t65298671;

#include "codegen/il2cpp-codegen.h"

// System.Void Completed.BoardManager/Count::.ctor(System.Int32,System.Int32)
extern "C"  void Count__ctor_m1195926005 (Count_t65298671 * __this, int32_t ___min, int32_t ___max, const MethodInfo* method) IL2CPP_METHOD_ATTR;
