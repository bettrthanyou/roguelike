﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Completed.Loader
struct Loader_t462637308;

#include "codegen/il2cpp-codegen.h"

// System.Void Completed.Loader::.ctor()
extern "C"  void Loader__ctor_m943804933 (Loader_t462637308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.Loader::Awake()
extern "C"  void Loader_Awake_m1181410152 (Loader_t462637308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
