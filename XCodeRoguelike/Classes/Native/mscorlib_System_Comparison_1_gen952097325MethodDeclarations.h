﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen3540781296MethodDeclarations.h"

// System.Void System.Comparison`1<Completed.Enemy>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m1097249420(__this, ___object, ___method, method) ((  void (*) (Comparison_1_t952097325 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m487232819_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<Completed.Enemy>::Invoke(T,T)
#define Comparison_1_Invoke_m1167475508(__this, ___x, ___y, method) ((  int32_t (*) (Comparison_1_t952097325 *, Enemy_t2543389745 *, Enemy_t2543389745 *, const MethodInfo*))Comparison_1_Invoke_m1888033133_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<Completed.Enemy>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m4098110125(__this, ___x, ___y, ___callback, ___object, method) ((  Il2CppObject * (*) (Comparison_1_t952097325 *, Enemy_t2543389745 *, Enemy_t2543389745 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3177996774_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<Completed.Enemy>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m248223928(__this, ___result, method) ((  int32_t (*) (Comparison_1_t952097325 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m651541983_gshared)(__this, ___result, method)
