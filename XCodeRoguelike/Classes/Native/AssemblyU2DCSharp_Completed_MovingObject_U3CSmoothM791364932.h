﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// Completed.MovingObject
struct MovingObject_t1424344598;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Completed.MovingObject/<SmoothMovement>c__Iterator0
struct  U3CSmoothMovementU3Ec__Iterator0_t791364932  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 Completed.MovingObject/<SmoothMovement>c__Iterator0::end
	Vector3_t3525329789  ___end_0;
	// System.Single Completed.MovingObject/<SmoothMovement>c__Iterator0::<sqrRemainingDistance>__0
	float ___U3CsqrRemainingDistanceU3E__0_1;
	// UnityEngine.Vector3 Completed.MovingObject/<SmoothMovement>c__Iterator0::<newPostion>__1
	Vector3_t3525329789  ___U3CnewPostionU3E__1_2;
	// System.Int32 Completed.MovingObject/<SmoothMovement>c__Iterator0::$PC
	int32_t ___U24PC_3;
	// System.Object Completed.MovingObject/<SmoothMovement>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// UnityEngine.Vector3 Completed.MovingObject/<SmoothMovement>c__Iterator0::<$>end
	Vector3_t3525329789  ___U3CU24U3Eend_5;
	// Completed.MovingObject Completed.MovingObject/<SmoothMovement>c__Iterator0::<>f__this
	MovingObject_t1424344598 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_end_0() { return static_cast<int32_t>(offsetof(U3CSmoothMovementU3Ec__Iterator0_t791364932, ___end_0)); }
	inline Vector3_t3525329789  get_end_0() const { return ___end_0; }
	inline Vector3_t3525329789 * get_address_of_end_0() { return &___end_0; }
	inline void set_end_0(Vector3_t3525329789  value)
	{
		___end_0 = value;
	}

	inline static int32_t get_offset_of_U3CsqrRemainingDistanceU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSmoothMovementU3Ec__Iterator0_t791364932, ___U3CsqrRemainingDistanceU3E__0_1)); }
	inline float get_U3CsqrRemainingDistanceU3E__0_1() const { return ___U3CsqrRemainingDistanceU3E__0_1; }
	inline float* get_address_of_U3CsqrRemainingDistanceU3E__0_1() { return &___U3CsqrRemainingDistanceU3E__0_1; }
	inline void set_U3CsqrRemainingDistanceU3E__0_1(float value)
	{
		___U3CsqrRemainingDistanceU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CnewPostionU3E__1_2() { return static_cast<int32_t>(offsetof(U3CSmoothMovementU3Ec__Iterator0_t791364932, ___U3CnewPostionU3E__1_2)); }
	inline Vector3_t3525329789  get_U3CnewPostionU3E__1_2() const { return ___U3CnewPostionU3E__1_2; }
	inline Vector3_t3525329789 * get_address_of_U3CnewPostionU3E__1_2() { return &___U3CnewPostionU3E__1_2; }
	inline void set_U3CnewPostionU3E__1_2(Vector3_t3525329789  value)
	{
		___U3CnewPostionU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CSmoothMovementU3Ec__Iterator0_t791364932, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CSmoothMovementU3Ec__Iterator0_t791364932, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eend_5() { return static_cast<int32_t>(offsetof(U3CSmoothMovementU3Ec__Iterator0_t791364932, ___U3CU24U3Eend_5)); }
	inline Vector3_t3525329789  get_U3CU24U3Eend_5() const { return ___U3CU24U3Eend_5; }
	inline Vector3_t3525329789 * get_address_of_U3CU24U3Eend_5() { return &___U3CU24U3Eend_5; }
	inline void set_U3CU24U3Eend_5(Vector3_t3525329789  value)
	{
		___U3CU24U3Eend_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CSmoothMovementU3Ec__Iterator0_t791364932, ___U3CU3Ef__this_6)); }
	inline MovingObject_t1424344598 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline MovingObject_t1424344598 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(MovingObject_t1424344598 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
