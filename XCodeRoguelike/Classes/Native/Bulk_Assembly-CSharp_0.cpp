﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Completed.BoardManager
struct BoardManager_t3721644752;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3499186955;
// Completed.BoardManager/Count
struct Count_t65298671;
// Completed.Enemy
struct Enemy_t2543389745;
// UnityEngine.Animator
struct Animator_t792326996;
// System.Object
struct Il2CppObject;
// Completed.GameManager
struct GameManager_t550910980;
// UnityEngine.UI.Text
struct Text_t3286458198;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// Completed.GameManager/<MoveEnemies>c__Iterator1
struct U3CMoveEnemiesU3Ec__Iterator1_t3054131685;
// Completed.Loader
struct Loader_t462637308;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// Completed.MovingObject
struct MovingObject_t1424344598;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t262790558;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t3632243084;
// Completed.MovingObject/<SmoothMovement>c__Iterator0
struct U3CSmoothMovementU3Ec__Iterator0_t791364932;
// Completed.Player
struct Player_t574403530;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;
// Completed.SoundManager
struct SoundManager_t625664135;
// UnityEngine.AudioClip
struct AudioClip_t3714538611;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t2889538658;
// Completed.Wall
struct Wall_t2478977715;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t2223784725;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_Completed_BoardManager3721644752.h"
#include "AssemblyU2DCSharp_Completed_BoardManager3721644752MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "AssemblyU2DCSharp_Completed_BoardManager_Count65298671MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen27321462MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "AssemblyU2DCSharp_Completed_BoardManager_Count65298671.h"
#include "mscorlib_System_Collections_Generic_List_1_gen27321462.h"
#include "UnityEngine_UnityEngine_Vector33525329789MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "mscorlib_System_Single958209021.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3963434288MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "UnityEngine_UnityEngine_Mathf1597001355MethodDeclarations.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "AssemblyU2DCSharp_Completed_Enemy2543389745.h"
#include "AssemblyU2DCSharp_Completed_Enemy2543389745MethodDeclarations.h"
#include "AssemblyU2DCSharp_Completed_MovingObject1424344598MethodDeclarations.h"
#include "AssemblyU2DCSharp_Completed_GameManager550910980MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "AssemblyU2DCSharp_Completed_GameManager550910980.h"
#include "UnityEngine_UnityEngine_Animator792326996.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "AssemblyU2DCSharp_Completed_MovingObject1424344598.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3340348714MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3340348714.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"
#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine2246592261.h"
#include "UnityEngine_UnityEngine_Behaviour3120504042MethodDeclarations.h"
#include "AssemblyU2DCSharp_Completed_GameManager_U3CMoveEne3054131685MethodDeclarations.h"
#include "AssemblyU2DCSharp_Completed_GameManager_U3CMoveEne3054131685.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1291133240MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1291133240.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "AssemblyU2DCSharp_Completed_Loader462637308.h"
#include "AssemblyU2DCSharp_Completed_Loader462637308MethodDeclarations.h"
#include "AssemblyU2DCSharp_Completed_SoundManager625664135.h"
#include "AssemblyU2DCSharp_Completed_SoundManager625664135MethodDeclarations.h"
#include "UnityEngine_UnityEngine_BoxCollider2D262790558.h"
#include "UnityEngine_UnityEngine_Rigidbody2D3632243084.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4082783401.h"
#include "UnityEngine_UnityEngine_Vector23525329788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LayerMask1862190090MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics2D2930244358MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4082783401MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "UnityEngine_UnityEngine_LayerMask1862190090.h"
#include "AssemblyU2DCSharp_Completed_MovingObject_U3CSmoothM791364932MethodDeclarations.h"
#include "AssemblyU2DCSharp_Completed_MovingObject_U3CSmoothM791364932.h"
#include "UnityEngine_UnityEngine_Rigidbody2D3632243084MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time1525492538MethodDeclarations.h"
#include "AssemblyU2DCSharp_Completed_Player574403530.h"
#include "AssemblyU2DCSharp_Completed_Player574403530MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1593691127MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch1603883884MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch1603883884.h"
#include "UnityEngine_UnityEngine_TouchPhase1905076713.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_AudioClip3714538611.h"
#include "UnityEngine_UnityEngine_Application450040189MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator792326996MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource3628549054MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource3628549054.h"
#include "AssemblyU2DCSharp_Completed_Wall2478977715.h"
#include "AssemblyU2DCSharp_Completed_Wall2478977715MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer2223784725.h"
#include "UnityEngine_UnityEngine_SpriteRenderer2223784725MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite4006040370.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t792326996_m4147395588(__this, method) ((  Animator_t792326996 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// System.Void Completed.Enemy::AttemptMove<System.Object>(System.Int32,System.Int32)
extern "C"  void Enemy_AttemptMove_TisIl2CppObject_m1551278378_gshared (Enemy_t2543389745 * __this, int32_t p0, int32_t p1, const MethodInfo* method);
#define Enemy_AttemptMove_TisIl2CppObject_m1551278378(__this, p0, p1, method) ((  void (*) (Enemy_t2543389745 *, int32_t, int32_t, const MethodInfo*))Enemy_AttemptMove_TisIl2CppObject_m1551278378_gshared)(__this, p0, p1, method)
// System.Void Completed.Enemy::AttemptMove<Completed.Player>(System.Int32,System.Int32)
#define Enemy_AttemptMove_TisPlayer_t574403530_m2944160326(__this, p0, p1, method) ((  void (*) (Enemy_t2543389745 *, int32_t, int32_t, const MethodInfo*))Enemy_AttemptMove_TisIl2CppObject_m1551278378_gshared)(__this, p0, p1, method)
// !!0 UnityEngine.Component::GetComponent<Completed.BoardManager>()
#define Component_GetComponent_TisBoardManager_t3721644752_m743185115(__this, method) ((  BoardManager_t3721644752 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t3286458198_m202917489(__this, method) ((  Text_t3286458198 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3133387403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3133387403(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t4012695102_m3917608929(__this /* static, unused */, p0, method) ((  GameObject_t4012695102 * (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider2D>()
#define Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519(__this, method) ((  BoxCollider2D_t262790558 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
#define Component_GetComponent_TisRigidbody2D_t3632243084_m2201104241(__this, method) ((  Rigidbody2D_t3632243084 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// System.Void Completed.Player::AttemptMove<System.Object>(System.Int32,System.Int32)
extern "C"  void Player_AttemptMove_TisIl2CppObject_m1853916987_gshared (Player_t574403530 * __this, int32_t p0, int32_t p1, const MethodInfo* method);
#define Player_AttemptMove_TisIl2CppObject_m1853916987(__this, p0, p1, method) ((  void (*) (Player_t574403530 *, int32_t, int32_t, const MethodInfo*))Player_AttemptMove_TisIl2CppObject_m1853916987_gshared)(__this, p0, p1, method)
// System.Void Completed.Player::AttemptMove<Completed.Wall>(System.Int32,System.Int32)
#define Player_AttemptMove_TisWall_t2478977715_m3215699262(__this, p0, p1, method) ((  void (*) (Player_t574403530 *, int32_t, int32_t, const MethodInfo*))Player_AttemptMove_TisIl2CppObject_m1853916987_gshared)(__this, p0, p1, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t2223784725_m4090179846(__this, method) ((  SpriteRenderer_t2223784725 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Completed.BoardManager::.ctor()
extern TypeInfo* Count_t65298671_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t27321462_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2803994520_MethodInfo_var;
extern const uint32_t BoardManager__ctor_m1357925105_MetadataUsageId;
extern "C"  void BoardManager__ctor_m1357925105 (BoardManager_t3721644752 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BoardManager__ctor_m1357925105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_columns_2(8);
		__this->set_rows_3(8);
		Count_t65298671 * L_0 = (Count_t65298671 *)il2cpp_codegen_object_new(Count_t65298671_il2cpp_TypeInfo_var);
		Count__ctor_m1195926005(L_0, 5, ((int32_t)9), /*hidden argument*/NULL);
		__this->set_wallCount_4(L_0);
		Count_t65298671 * L_1 = (Count_t65298671 *)il2cpp_codegen_object_new(Count_t65298671_il2cpp_TypeInfo_var);
		Count__ctor_m1195926005(L_1, 1, 5, /*hidden argument*/NULL);
		__this->set_foodCount_5(L_1);
		List_1_t27321462 * L_2 = (List_1_t27321462 *)il2cpp_codegen_object_new(List_1_t27321462_il2cpp_TypeInfo_var);
		List_1__ctor_m2803994520(L_2, /*hidden argument*/List_1__ctor_m2803994520_MethodInfo_var);
		__this->set_gridPositions_13(L_2);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.BoardManager::InitialiseList()
extern "C"  void BoardManager_InitialiseList_m1029667944 (BoardManager_t3721644752 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		List_1_t27321462 * L_0 = __this->get_gridPositions_13();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear() */, L_0);
		V_0 = 1;
		goto IL_0048;
	}

IL_0012:
	{
		V_1 = 1;
		goto IL_0036;
	}

IL_0019:
	{
		List_1_t27321462 * L_1 = __this->get_gridPositions_13();
		int32_t L_2 = V_0;
		int32_t L_3 = V_1;
		Vector3_t3525329789  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m2926210380(&L_4, (((float)((float)L_2))), (((float)((float)L_3))), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< Vector3_t3525329789  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_1, L_4);
		int32_t L_5 = V_1;
		V_1 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = __this->get_rows_3();
		if ((((int32_t)L_6) < ((int32_t)((int32_t)((int32_t)L_7-(int32_t)1)))))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0048:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = __this->get_columns_2();
		if ((((int32_t)L_9) < ((int32_t)((int32_t)((int32_t)L_10-(int32_t)1)))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void Completed.BoardManager::BoardSetup()
extern TypeInfo* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral64356038;
extern const uint32_t BoardManager_BoardSetup_m2016992298_MetadataUsageId;
extern "C"  void BoardManager_BoardSetup_m2016992298 (BoardManager_t3721644752 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BoardManager_BoardSetup_m2016992298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	GameObject_t4012695102 * V_2 = NULL;
	GameObject_t4012695102 * V_3 = NULL;
	{
		GameObject_t4012695102 * L_0 = (GameObject_t4012695102 *)il2cpp_codegen_object_new(GameObject_t4012695102_il2cpp_TypeInfo_var);
		GameObject__ctor_m3920833606(L_0, _stringLiteral64356038, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t284553113 * L_1 = GameObject_get_transform_m1278640159(L_0, /*hidden argument*/NULL);
		__this->set_boardHolder_12(L_1);
		V_0 = (-1);
		goto IL_00bb;
	}

IL_001c:
	{
		V_1 = (-1);
		goto IL_00a9;
	}

IL_0023:
	{
		GameObjectU5BU5D_t3499186955* L_2 = __this->get_floorTiles_7();
		GameObjectU5BU5D_t3499186955* L_3 = __this->get_floorTiles_7();
		NullCheck(L_3);
		int32_t L_4 = Random_Range_m75452833(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_4);
		int32_t L_5 = L_4;
		V_2 = ((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) == ((int32_t)(-1))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_7 = V_0;
		int32_t L_8 = __this->get_columns_2();
		if ((((int32_t)L_7) == ((int32_t)L_8)))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) == ((int32_t)(-1))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_10 = V_1;
		int32_t L_11 = __this->get_rows_3();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_0075;
		}
	}

IL_005f:
	{
		GameObjectU5BU5D_t3499186955* L_12 = __this->get_outerWallTiles_11();
		GameObjectU5BU5D_t3499186955* L_13 = __this->get_outerWallTiles_11();
		NullCheck(L_13);
		int32_t L_14 = Random_Range_m75452833(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_14);
		int32_t L_15 = L_14;
		V_2 = ((L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_15)));
	}

IL_0075:
	{
		GameObject_t4012695102 * L_16 = V_2;
		int32_t L_17 = V_0;
		int32_t L_18 = V_1;
		Vector3_t3525329789  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m2926210380(&L_19, (((float)((float)L_17))), (((float)((float)L_18))), (0.0f), /*hidden argument*/NULL);
		Quaternion_t1891715979  L_20 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_21 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_16, L_19, L_20, /*hidden argument*/NULL);
		V_3 = ((GameObject_t4012695102 *)IsInstSealed(L_21, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_22 = V_3;
		NullCheck(L_22);
		Transform_t284553113 * L_23 = GameObject_get_transform_m1278640159(L_22, /*hidden argument*/NULL);
		Transform_t284553113 * L_24 = __this->get_boardHolder_12();
		NullCheck(L_23);
		Transform_SetParent_m3449663462(L_23, L_24, /*hidden argument*/NULL);
		int32_t L_25 = V_1;
		V_1 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_00a9:
	{
		int32_t L_26 = V_1;
		int32_t L_27 = __this->get_rows_3();
		if ((((int32_t)L_26) < ((int32_t)((int32_t)((int32_t)L_27+(int32_t)1)))))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_28 = V_0;
		V_0 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_00bb:
	{
		int32_t L_29 = V_0;
		int32_t L_30 = __this->get_columns_2();
		if ((((int32_t)L_29) < ((int32_t)((int32_t)((int32_t)L_30+(int32_t)1)))))
		{
			goto IL_001c;
		}
	}
	{
		return;
	}
}
// UnityEngine.Vector3 Completed.BoardManager::RandomPosition()
extern "C"  Vector3_t3525329789  BoardManager_RandomPosition_m273627449 (BoardManager_t3721644752 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		List_1_t27321462 * L_0 = __this->get_gridPositions_13();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count() */, L_0);
		int32_t L_2 = Random_Range_m75452833(NULL /*static, unused*/, 0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		List_1_t27321462 * L_3 = __this->get_gridPositions_13();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Vector3_t3525329789  L_5 = VirtFuncInvoker1< Vector3_t3525329789 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_3, L_4);
		V_1 = L_5;
		List_1_t27321462 * L_6 = __this->get_gridPositions_13();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveAt(System.Int32) */, L_6, L_7);
		Vector3_t3525329789  L_8 = V_1;
		return L_8;
	}
}
// System.Void Completed.BoardManager::LayoutObjectAtRandom(UnityEngine.GameObject[],System.Int32,System.Int32)
extern "C"  void BoardManager_LayoutObjectAtRandom_m2822549352 (BoardManager_t3721644752 * __this, GameObjectU5BU5D_t3499186955* ___tileArray, int32_t ___minimum, int32_t ___maximum, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	GameObject_t4012695102 * V_3 = NULL;
	{
		int32_t L_0 = ___minimum;
		int32_t L_1 = ___maximum;
		int32_t L_2 = Random_Range_m75452833(NULL /*static, unused*/, L_0, ((int32_t)((int32_t)L_1+(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = 0;
		goto IL_0035;
	}

IL_0011:
	{
		Vector3_t3525329789  L_3 = BoardManager_RandomPosition_m273627449(__this, /*hidden argument*/NULL);
		V_2 = L_3;
		GameObjectU5BU5D_t3499186955* L_4 = ___tileArray;
		GameObjectU5BU5D_t3499186955* L_5 = ___tileArray;
		NullCheck(L_5);
		int32_t L_6 = Random_Range_m75452833(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_6);
		int32_t L_7 = L_6;
		V_3 = ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_7)));
		GameObject_t4012695102 * L_8 = V_3;
		Vector3_t3525329789  L_9 = V_2;
		Quaternion_t1891715979  L_10 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0035:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0011;
		}
	}
	{
		return;
	}
}
// System.Void Completed.BoardManager::SetupScene(System.Int32)
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t BoardManager_SetupScene_m1053917523_MetadataUsageId;
extern "C"  void BoardManager_SetupScene_m1053917523 (BoardManager_t3721644752 * __this, int32_t ___level, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BoardManager_SetupScene_m1053917523_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		BoardManager_BoardSetup_m2016992298(__this, /*hidden argument*/NULL);
		BoardManager_InitialiseList_m1029667944(__this, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3499186955* L_0 = __this->get_wallTiles_8();
		Count_t65298671 * L_1 = __this->get_wallCount_4();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_minimum_0();
		Count_t65298671 * L_3 = __this->get_wallCount_4();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_maximum_1();
		BoardManager_LayoutObjectAtRandom_m2822549352(__this, L_0, L_2, L_4, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3499186955* L_5 = __this->get_foodTiles_9();
		Count_t65298671 * L_6 = __this->get_foodCount_5();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_minimum_0();
		Count_t65298671 * L_8 = __this->get_foodCount_5();
		NullCheck(L_8);
		int32_t L_9 = L_8->get_maximum_1();
		BoardManager_LayoutObjectAtRandom_m2822549352(__this, L_5, L_7, L_9, /*hidden argument*/NULL);
		int32_t L_10 = ___level;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_11 = Mathf_Log_m2062790663(NULL /*static, unused*/, (((float)((float)L_10))), (2.0f), /*hidden argument*/NULL);
		V_0 = (((int32_t)((int32_t)L_11)));
		GameObjectU5BU5D_t3499186955* L_12 = __this->get_enemyTiles_10();
		int32_t L_13 = V_0;
		int32_t L_14 = V_0;
		BoardManager_LayoutObjectAtRandom_m2822549352(__this, L_12, L_13, L_14, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_15 = __this->get_exit_6();
		int32_t L_16 = __this->get_columns_2();
		int32_t L_17 = __this->get_rows_3();
		Vector3_t3525329789  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2926210380(&L_18, (((float)((float)((int32_t)((int32_t)L_16-(int32_t)1))))), (((float)((float)((int32_t)((int32_t)L_17-(int32_t)1))))), (0.0f), /*hidden argument*/NULL);
		Quaternion_t1891715979  L_19 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_15, L_18, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.BoardManager/Count::.ctor(System.Int32,System.Int32)
extern "C"  void Count__ctor_m1195926005 (Count_t65298671 * __this, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___min;
		__this->set_minimum_0(L_0);
		int32_t L_1 = ___max;
		__this->set_maximum_1(L_1);
		return;
	}
}
// System.Void Completed.Enemy::.ctor()
extern "C"  void Enemy__ctor_m36979494 (Enemy_t2543389745 * __this, const MethodInfo* method)
{
	{
		MovingObject__ctor_m184393323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.Enemy::Start()
extern TypeInfo* GameManager_t550910980_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimator_t792326996_m4147395588_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern const uint32_t Enemy_Start_m3279084582_MetadataUsageId;
extern "C"  void Enemy_Start_m3279084582 (Enemy_t2543389745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enemy_Start_m3279084582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t550910980_il2cpp_TypeInfo_var);
		GameManager_t550910980 * L_0 = ((GameManager_t550910980_StaticFields*)GameManager_t550910980_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		NullCheck(L_0);
		GameManager_AddEnemyToList_m2494537628(L_0, __this, /*hidden argument*/NULL);
		Animator_t792326996 * L_1 = Component_GetComponent_TisAnimator_t792326996_m4147395588(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t792326996_m4147395588_MethodInfo_var);
		__this->set_animator_10(L_1);
		GameObject_t4012695102 * L_2 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral2393081601, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t284553113 * L_3 = GameObject_get_transform_m1278640159(L_2, /*hidden argument*/NULL);
		__this->set_target_11(L_3);
		MovingObject_Start_m3426498411(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.Enemy::MoveEnemy()
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const MethodInfo* Enemy_AttemptMove_TisPlayer_t574403530_m2944160326_MethodInfo_var;
extern const uint32_t Enemy_MoveEnemy_m1036909371_MetadataUsageId;
extern "C"  void Enemy_MoveEnemy_m1036909371 (Enemy_t2543389745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enemy_MoveEnemy_m1036909371_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3525329789  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3525329789  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t3525329789  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3525329789  V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t G_B4_0 = 0;
	int32_t G_B8_0 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		Transform_t284553113 * L_0 = __this->get_target_11();
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		V_2 = L_1;
		float L_2 = (&V_2)->get_x_1();
		Transform_t284553113 * L_3 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3525329789  L_4 = Transform_get_position_m2211398607(L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		float L_5 = (&V_3)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_6 = fabsf(((float)((float)L_2-(float)L_5)));
		if ((!(((float)L_6) < ((float)(1.401298E-45f)))))
		{
			goto IL_0074;
		}
	}
	{
		Transform_t284553113 * L_7 = __this->get_target_11();
		NullCheck(L_7);
		Vector3_t3525329789  L_8 = Transform_get_position_m2211398607(L_7, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = (&V_4)->get_y_2();
		Transform_t284553113 * L_10 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t3525329789  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		V_5 = L_11;
		float L_12 = (&V_5)->get_y_2();
		if ((!(((float)L_9) > ((float)L_12))))
		{
			goto IL_006d;
		}
	}
	{
		G_B4_0 = 1;
		goto IL_006e;
	}

IL_006d:
	{
		G_B4_0 = (-1);
	}

IL_006e:
	{
		V_1 = G_B4_0;
		goto IL_00a9;
	}

IL_0074:
	{
		Transform_t284553113 * L_13 = __this->get_target_11();
		NullCheck(L_13);
		Vector3_t3525329789  L_14 = Transform_get_position_m2211398607(L_13, /*hidden argument*/NULL);
		V_6 = L_14;
		float L_15 = (&V_6)->get_x_1();
		Transform_t284553113 * L_16 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t3525329789  L_17 = Transform_get_position_m2211398607(L_16, /*hidden argument*/NULL);
		V_7 = L_17;
		float L_18 = (&V_7)->get_x_1();
		if ((!(((float)L_15) > ((float)L_18))))
		{
			goto IL_00a7;
		}
	}
	{
		G_B8_0 = 1;
		goto IL_00a8;
	}

IL_00a7:
	{
		G_B8_0 = (-1);
	}

IL_00a8:
	{
		V_0 = G_B8_0;
	}

IL_00a9:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = V_1;
		GenericVirtActionInvoker2< int32_t, int32_t >::Invoke(Enemy_AttemptMove_TisPlayer_t574403530_m2944160326_MethodInfo_var, __this, L_19, L_20);
		return;
	}
}
// System.Void Completed.GameManager::.ctor()
extern "C"  void GameManager__ctor_m3445685363 (GameManager_t550910980 * __this, const MethodInfo* method)
{
	{
		__this->set_levelStartDelay_2((2.0f));
		__this->set_turnDelay_3((0.1f));
		__this->set_playerFoodPoints_4(((int32_t)100));
		__this->set_playersTurn_6((bool)1);
		__this->set_level_10(1);
		__this->set_doingSetup_13((bool)1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.GameManager::.cctor()
extern "C"  void GameManager__cctor_m3254934938 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Completed.GameManager::Awake()
extern TypeInfo* GameManager_t550910980_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t3340348714_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2837633348_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisBoardManager_t3721644752_m743185115_MethodInfo_var;
extern const uint32_t GameManager_Awake_m3683290582_MetadataUsageId;
extern "C"  void GameManager_Awake_m3683290582 (GameManager_t550910980 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameManager_Awake_m3683290582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t550910980_il2cpp_TypeInfo_var);
		GameManager_t550910980 * L_0 = ((GameManager_t550910980_StaticFields*)GameManager_t550910980_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t550910980_il2cpp_TypeInfo_var);
		((GameManager_t550910980_StaticFields*)GameManager_t550910980_il2cpp_TypeInfo_var->static_fields)->set_instance_5(__this);
		goto IL_0036;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t550910980_il2cpp_TypeInfo_var);
		GameManager_t550910980 * L_2 = ((GameManager_t550910980_StaticFields*)GameManager_t550910980_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, __this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0036:
	{
		GameObject_t4012695102 * L_5 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m4064482788(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		List_1_t3340348714 * L_6 = (List_1_t3340348714 *)il2cpp_codegen_object_new(List_1_t3340348714_il2cpp_TypeInfo_var);
		List_1__ctor_m2837633348(L_6, /*hidden argument*/List_1__ctor_m2837633348_MethodInfo_var);
		__this->set_enemies_11(L_6);
		BoardManager_t3721644752 * L_7 = Component_GetComponent_TisBoardManager_t3721644752_m743185115(__this, /*hidden argument*/Component_GetComponent_TisBoardManager_t3721644752_m743185115_MethodInfo_var);
		__this->set_boardScript_9(L_7);
		GameManager_InitGame_m2546400019(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.GameManager::OnLevelWasLoaded(System.Int32)
extern "C"  void GameManager_OnLevelWasLoaded_m3512839019 (GameManager_t550910980 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_level_10();
		__this->set_level_10(((int32_t)((int32_t)L_0+(int32_t)1)));
		GameManager_InitGame_m2546400019(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.GameManager::InitGame()
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t3286458198_m202917489_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2180025431;
extern Il2CppCodeGenString* _stringLiteral347738801;
extern Il2CppCodeGenString* _stringLiteral2122788;
extern Il2CppCodeGenString* _stringLiteral2611874617;
extern const uint32_t GameManager_InitGame_m2546400019_MetadataUsageId;
extern "C"  void GameManager_InitGame_m2546400019 (GameManager_t550910980 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameManager_InitGame_m2546400019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_doingSetup_13((bool)1);
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2180025431, /*hidden argument*/NULL);
		__this->set_levelImage_8(L_0);
		GameObject_t4012695102 * L_1 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral347738801, /*hidden argument*/NULL);
		NullCheck(L_1);
		Text_t3286458198 * L_2 = GameObject_GetComponent_TisText_t3286458198_m202917489(L_1, /*hidden argument*/GameObject_GetComponent_TisText_t3286458198_m202917489_MethodInfo_var);
		__this->set_levelText_7(L_2);
		Text_t3286458198 * L_3 = __this->get_levelText_7();
		int32_t L_4 = __this->get_level_10();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2122788, L_6, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_7);
		GameObject_t4012695102 * L_8 = __this->get_levelImage_8();
		NullCheck(L_8);
		GameObject_SetActive_m3538205401(L_8, (bool)1, /*hidden argument*/NULL);
		float L_9 = __this->get_levelStartDelay_2();
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral2611874617, L_9, /*hidden argument*/NULL);
		List_1_t3340348714 * L_10 = __this->get_enemies_11();
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<Completed.Enemy>::Clear() */, L_10);
		BoardManager_t3721644752 * L_11 = __this->get_boardScript_9();
		int32_t L_12 = __this->get_level_10();
		NullCheck(L_11);
		BoardManager_SetupScene_m1053917523(L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.GameManager::HideLevelImage()
extern "C"  void GameManager_HideLevelImage_m3545671498 (GameManager_t550910980 * __this, const MethodInfo* method)
{
	{
		GameObject_t4012695102 * L_0 = __this->get_levelImage_8();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)0, /*hidden argument*/NULL);
		__this->set_doingSetup_13((bool)0);
		return;
	}
}
// System.Void Completed.GameManager::Update()
extern "C"  void GameManager_Update_m1168925946 (GameManager_t550910980 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_playersTurn_6();
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		bool L_1 = __this->get_enemiesMoving_12();
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		bool L_2 = __this->get_doingSetup_13();
		if (!L_2)
		{
			goto IL_0022;
		}
	}

IL_0021:
	{
		return;
	}

IL_0022:
	{
		Il2CppObject * L_3 = GameManager_MoveEnemies_m1594768302(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.GameManager::AddEnemyToList(Completed.Enemy)
extern "C"  void GameManager_AddEnemyToList_m2494537628 (GameManager_t550910980 * __this, Enemy_t2543389745 * ___script, const MethodInfo* method)
{
	{
		List_1_t3340348714 * L_0 = __this->get_enemies_11();
		Enemy_t2543389745 * L_1 = ___script;
		NullCheck(L_0);
		VirtActionInvoker1< Enemy_t2543389745 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Completed.Enemy>::Add(!0) */, L_0, L_1);
		return;
	}
}
// System.Void Completed.GameManager::GameOver()
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1958650340;
extern Il2CppCodeGenString* _stringLiteral1533326103;
extern const uint32_t GameManager_GameOver_m3163353303_MetadataUsageId;
extern "C"  void GameManager_GameOver_m3163353303 (GameManager_t550910980 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameManager_GameOver_m3163353303_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Text_t3286458198 * L_0 = __this->get_levelText_7();
		int32_t L_1 = __this->get_level_10();
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral1958650340, L_3, _stringLiteral1533326103, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		GameObject_t4012695102 * L_5 = __this->get_levelImage_8();
		NullCheck(L_5);
		GameObject_SetActive_m3538205401(L_5, (bool)1, /*hidden argument*/NULL);
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Completed.GameManager::MoveEnemies()
extern TypeInfo* U3CMoveEnemiesU3Ec__Iterator1_t3054131685_il2cpp_TypeInfo_var;
extern const uint32_t GameManager_MoveEnemies_m1594768302_MetadataUsageId;
extern "C"  Il2CppObject * GameManager_MoveEnemies_m1594768302 (GameManager_t550910980 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameManager_MoveEnemies_m1594768302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CMoveEnemiesU3Ec__Iterator1_t3054131685 * V_0 = NULL;
	{
		U3CMoveEnemiesU3Ec__Iterator1_t3054131685 * L_0 = (U3CMoveEnemiesU3Ec__Iterator1_t3054131685 *)il2cpp_codegen_object_new(U3CMoveEnemiesU3Ec__Iterator1_t3054131685_il2cpp_TypeInfo_var);
		U3CMoveEnemiesU3Ec__Iterator1__ctor_m2976783517(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CMoveEnemiesU3Ec__Iterator1_t3054131685 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CMoveEnemiesU3Ec__Iterator1_t3054131685 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Completed.GameManager/<MoveEnemies>c__Iterator1::.ctor()
extern "C"  void U3CMoveEnemiesU3Ec__Iterator1__ctor_m2976783517 (U3CMoveEnemiesU3Ec__Iterator1_t3054131685 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Completed.GameManager/<MoveEnemies>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMoveEnemiesU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1849018581 (U3CMoveEnemiesU3Ec__Iterator1_t3054131685 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object Completed.GameManager/<MoveEnemies>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMoveEnemiesU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3296073321 (U3CMoveEnemiesU3Ec__Iterator1_t3054131685 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean Completed.GameManager/<MoveEnemies>c__Iterator1::MoveNext()
extern TypeInfo* WaitForSeconds_t1291133240_il2cpp_TypeInfo_var;
extern const uint32_t U3CMoveEnemiesU3Ec__Iterator1_MoveNext_m3174090807_MetadataUsageId;
extern "C"  bool U3CMoveEnemiesU3Ec__Iterator1_MoveNext_m3174090807 (U3CMoveEnemiesU3Ec__Iterator1_t3054131685 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMoveEnemiesU3Ec__Iterator1_MoveNext_m3174090807_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0029;
		}
		if (L_1 == 1)
		{
			goto IL_0057;
		}
		if (L_1 == 2)
		{
			goto IL_008e;
		}
		if (L_1 == 3)
		{
			goto IL_00e7;
		}
	}
	{
		goto IL_012f;
	}

IL_0029:
	{
		GameManager_t550910980 * L_2 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		L_2->set_enemiesMoving_12((bool)1);
		GameManager_t550910980 * L_3 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_3);
		float L_4 = L_3->get_turnDelay_3();
		WaitForSeconds_t1291133240 * L_5 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_5, L_4, /*hidden argument*/NULL);
		__this->set_U24current_2(L_5);
		__this->set_U24PC_1(1);
		goto IL_0131;
	}

IL_0057:
	{
		GameManager_t550910980 * L_6 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_6);
		List_1_t3340348714 * L_7 = L_6->get_enemies_11();
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Completed.Enemy>::get_Count() */, L_7);
		if (L_8)
		{
			goto IL_008e;
		}
	}
	{
		GameManager_t550910980 * L_9 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		float L_10 = L_9->get_turnDelay_3();
		WaitForSeconds_t1291133240 * L_11 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_11, L_10, /*hidden argument*/NULL);
		__this->set_U24current_2(L_11);
		__this->set_U24PC_1(2);
		goto IL_0131;
	}

IL_008e:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_00f5;
	}

IL_009a:
	{
		GameManager_t550910980 * L_12 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_12);
		List_1_t3340348714 * L_13 = L_12->get_enemies_11();
		int32_t L_14 = __this->get_U3CiU3E__0_0();
		NullCheck(L_13);
		Enemy_t2543389745 * L_15 = VirtFuncInvoker1< Enemy_t2543389745 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Completed.Enemy>::get_Item(System.Int32) */, L_13, L_14);
		NullCheck(L_15);
		Enemy_MoveEnemy_m1036909371(L_15, /*hidden argument*/NULL);
		GameManager_t550910980 * L_16 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_16);
		List_1_t3340348714 * L_17 = L_16->get_enemies_11();
		int32_t L_18 = __this->get_U3CiU3E__0_0();
		NullCheck(L_17);
		Enemy_t2543389745 * L_19 = VirtFuncInvoker1< Enemy_t2543389745 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Completed.Enemy>::get_Item(System.Int32) */, L_17, L_18);
		NullCheck(L_19);
		float L_20 = ((MovingObject_t1424344598 *)L_19)->get_moveTime_2();
		WaitForSeconds_t1291133240 * L_21 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_21, L_20, /*hidden argument*/NULL);
		__this->set_U24current_2(L_21);
		__this->set_U24PC_1(3);
		goto IL_0131;
	}

IL_00e7:
	{
		int32_t L_22 = __this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_22+(int32_t)1)));
	}

IL_00f5:
	{
		int32_t L_23 = __this->get_U3CiU3E__0_0();
		GameManager_t550910980 * L_24 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_24);
		List_1_t3340348714 * L_25 = L_24->get_enemies_11();
		NullCheck(L_25);
		int32_t L_26 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Completed.Enemy>::get_Count() */, L_25);
		if ((((int32_t)L_23) < ((int32_t)L_26)))
		{
			goto IL_009a;
		}
	}
	{
		GameManager_t550910980 * L_27 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_27);
		L_27->set_playersTurn_6((bool)1);
		GameManager_t550910980 * L_28 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_28);
		L_28->set_enemiesMoving_12((bool)0);
		__this->set_U24PC_1((-1));
	}

IL_012f:
	{
		return (bool)0;
	}

IL_0131:
	{
		return (bool)1;
	}
	// Dead block : IL_0133: ldloc.1
}
// System.Void Completed.GameManager/<MoveEnemies>c__Iterator1::Dispose()
extern "C"  void U3CMoveEnemiesU3Ec__Iterator1_Dispose_m139023770 (U3CMoveEnemiesU3Ec__Iterator1_t3054131685 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void Completed.GameManager/<MoveEnemies>c__Iterator1::Reset()
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CMoveEnemiesU3Ec__Iterator1_Reset_m623216458_MetadataUsageId;
extern "C"  void U3CMoveEnemiesU3Ec__Iterator1_Reset_m623216458 (U3CMoveEnemiesU3Ec__Iterator1_t3054131685 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMoveEnemiesU3Ec__Iterator1_Reset_m623216458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Completed.Loader::.ctor()
extern "C"  void Loader__ctor_m943804933 (Loader_t462637308 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.Loader::Awake()
extern TypeInfo* GameManager_t550910980_il2cpp_TypeInfo_var;
extern TypeInfo* SoundManager_t625664135_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t4012695102_m3917608929_MethodInfo_var;
extern const uint32_t Loader_Awake_m1181410152_MetadataUsageId;
extern "C"  void Loader_Awake_m1181410152 (Loader_t462637308 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Loader_Awake_m1181410152_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t550910980_il2cpp_TypeInfo_var);
		GameManager_t550910980 * L_0 = ((GameManager_t550910980_StaticFields*)GameManager_t550910980_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		GameObject_t4012695102 * L_2 = __this->get_gameManager_2();
		Object_Instantiate_TisGameObject_t4012695102_m3917608929(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t4012695102_m3917608929_MethodInfo_var);
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t625664135_il2cpp_TypeInfo_var);
		SoundManager_t625664135 * L_3 = ((SoundManager_t625664135_StaticFields*)SoundManager_t625664135_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		GameObject_t4012695102 * L_5 = __this->get_soundManager_3();
		Object_Instantiate_TisGameObject_t4012695102_m3917608929(NULL /*static, unused*/, L_5, /*hidden argument*/Object_Instantiate_TisGameObject_t4012695102_m3917608929_MethodInfo_var);
	}

IL_0038:
	{
		return;
	}
}
// System.Void Completed.MovingObject::.ctor()
extern "C"  void MovingObject__ctor_m184393323 (MovingObject_t1424344598 * __this, const MethodInfo* method)
{
	{
		__this->set_moveTime_2((0.1f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.MovingObject::Start()
extern const MethodInfo* Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t3632243084_m2201104241_MethodInfo_var;
extern const uint32_t MovingObject_Start_m3426498411_MetadataUsageId;
extern "C"  void MovingObject_Start_m3426498411 (MovingObject_t1424344598 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MovingObject_Start_m3426498411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BoxCollider2D_t262790558 * L_0 = Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519_MethodInfo_var);
		__this->set_boxCollider_4(L_0);
		Rigidbody2D_t3632243084 * L_1 = Component_GetComponent_TisRigidbody2D_t3632243084_m2201104241(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t3632243084_m2201104241_MethodInfo_var);
		__this->set_rb2D_5(L_1);
		float L_2 = __this->get_moveTime_2();
		__this->set_inverseMoveTime_6(((float)((float)(1.0f)/(float)L_2)));
		return;
	}
}
// System.Boolean Completed.MovingObject::Move(System.Int32,System.Int32,UnityEngine.RaycastHit2D&)
extern TypeInfo* Physics2D_t2930244358_il2cpp_TypeInfo_var;
extern const uint32_t MovingObject_Move_m118298465_MetadataUsageId;
extern "C"  bool MovingObject_Move_m118298465 (MovingObject_t1424344598 * __this, int32_t ___xDir, int32_t ___yDir, RaycastHit2D_t4082783401 * ___hit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MovingObject_Move_m118298465_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t3525329788  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		Vector2_t3525329788  L_2 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector2_t3525329788  L_3 = V_0;
		int32_t L_4 = ___xDir;
		int32_t L_5 = ___yDir;
		Vector2_t3525329788  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m1517109030(&L_6, (((float)((float)L_4))), (((float)((float)L_5))), /*hidden argument*/NULL);
		Vector2_t3525329788  L_7 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		BoxCollider2D_t262790558 * L_8 = __this->get_boxCollider_4();
		NullCheck(L_8);
		Behaviour_set_enabled_m2046806933(L_8, (bool)0, /*hidden argument*/NULL);
		RaycastHit2D_t4082783401 * L_9 = ___hit;
		Vector2_t3525329788  L_10 = V_0;
		Vector2_t3525329788  L_11 = V_1;
		LayerMask_t1862190090  L_12 = __this->get_blockingLayer_3();
		int32_t L_13 = LayerMask_op_Implicit_m1595580047(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2930244358_il2cpp_TypeInfo_var);
		RaycastHit2D_t4082783401  L_14 = Physics2D_Linecast_m4170255972(NULL /*static, unused*/, L_10, L_11, L_13, /*hidden argument*/NULL);
		(*(RaycastHit2D_t4082783401 *)L_9) = L_14;
		BoxCollider2D_t262790558 * L_15 = __this->get_boxCollider_4();
		NullCheck(L_15);
		Behaviour_set_enabled_m2046806933(L_15, (bool)1, /*hidden argument*/NULL);
		RaycastHit2D_t4082783401 * L_16 = ___hit;
		Transform_t284553113 * L_17 = RaycastHit2D_get_transform_m1318597140(L_16, /*hidden argument*/NULL);
		bool L_18 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_17, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0077;
		}
	}
	{
		Vector2_t3525329788  L_19 = V_1;
		Vector3_t3525329789  L_20 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		Il2CppObject * L_21 = MovingObject_SmoothMovement_m1746373659(__this, L_20, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_21, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0077:
	{
		return (bool)0;
	}
}
// System.Collections.IEnumerator Completed.MovingObject::SmoothMovement(UnityEngine.Vector3)
extern TypeInfo* U3CSmoothMovementU3Ec__Iterator0_t791364932_il2cpp_TypeInfo_var;
extern const uint32_t MovingObject_SmoothMovement_m1746373659_MetadataUsageId;
extern "C"  Il2CppObject * MovingObject_SmoothMovement_m1746373659 (MovingObject_t1424344598 * __this, Vector3_t3525329789  ___end, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MovingObject_SmoothMovement_m1746373659_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSmoothMovementU3Ec__Iterator0_t791364932 * V_0 = NULL;
	{
		U3CSmoothMovementU3Ec__Iterator0_t791364932 * L_0 = (U3CSmoothMovementU3Ec__Iterator0_t791364932 *)il2cpp_codegen_object_new(U3CSmoothMovementU3Ec__Iterator0_t791364932_il2cpp_TypeInfo_var);
		U3CSmoothMovementU3Ec__Iterator0__ctor_m3383116808(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSmoothMovementU3Ec__Iterator0_t791364932 * L_1 = V_0;
		Vector3_t3525329789  L_2 = ___end;
		NullCheck(L_1);
		L_1->set_end_0(L_2);
		U3CSmoothMovementU3Ec__Iterator0_t791364932 * L_3 = V_0;
		Vector3_t3525329789  L_4 = ___end;
		NullCheck(L_3);
		L_3->set_U3CU24U3Eend_5(L_4);
		U3CSmoothMovementU3Ec__Iterator0_t791364932 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_6(__this);
		U3CSmoothMovementU3Ec__Iterator0_t791364932 * L_6 = V_0;
		return L_6;
	}
}
// System.Void Completed.MovingObject/<SmoothMovement>c__Iterator0::.ctor()
extern "C"  void U3CSmoothMovementU3Ec__Iterator0__ctor_m3383116808 (U3CSmoothMovementU3Ec__Iterator0_t791364932 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Completed.MovingObject/<SmoothMovement>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSmoothMovementU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m275464394 (U3CSmoothMovementU3Ec__Iterator0_t791364932 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object Completed.MovingObject/<SmoothMovement>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSmoothMovementU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3594262622 (U3CSmoothMovementU3Ec__Iterator0_t791364932 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Boolean Completed.MovingObject/<SmoothMovement>c__Iterator0::MoveNext()
extern "C"  bool U3CSmoothMovementU3Ec__Iterator0_MoveNext_m141991660 (U3CSmoothMovementU3Ec__Iterator0_t791364932 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	bool V_3 = false;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00dd;
		}
	}
	{
		goto IL_00f4;
	}

IL_0021:
	{
		MovingObject_t1424344598 * L_2 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_2);
		Transform_t284553113 * L_3 = Component_get_transform_m4257140443(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3525329789  L_4 = Transform_get_position_m2211398607(L_3, /*hidden argument*/NULL);
		Vector3_t3525329789  L_5 = __this->get_end_0();
		Vector3_t3525329789  L_6 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = Vector3_get_sqrMagnitude_m1207423764((&V_1), /*hidden argument*/NULL);
		__this->set_U3CsqrRemainingDistanceU3E__0_1(L_7);
		goto IL_00dd;
	}

IL_004f:
	{
		MovingObject_t1424344598 * L_8 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_8);
		Rigidbody2D_t3632243084 * L_9 = L_8->get_rb2D_5();
		NullCheck(L_9);
		Vector2_t3525329788  L_10 = Rigidbody2D_get_position_m3766784193(L_9, /*hidden argument*/NULL);
		Vector3_t3525329789  L_11 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Vector3_t3525329789  L_12 = __this->get_end_0();
		MovingObject_t1424344598 * L_13 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_13);
		float L_14 = L_13->get_inverseMoveTime_6();
		float L_15 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_16 = Vector3_MoveTowards_m2405650085(NULL /*static, unused*/, L_11, L_12, ((float)((float)L_14*(float)L_15)), /*hidden argument*/NULL);
		__this->set_U3CnewPostionU3E__1_2(L_16);
		MovingObject_t1424344598 * L_17 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_17);
		Rigidbody2D_t3632243084 * L_18 = L_17->get_rb2D_5();
		Vector3_t3525329789  L_19 = __this->get_U3CnewPostionU3E__1_2();
		Vector2_t3525329788  L_20 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		Rigidbody2D_MovePosition_m816157558(L_18, L_20, /*hidden argument*/NULL);
		MovingObject_t1424344598 * L_21 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_21);
		Transform_t284553113 * L_22 = Component_get_transform_m4257140443(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t3525329789  L_23 = Transform_get_position_m2211398607(L_22, /*hidden argument*/NULL);
		Vector3_t3525329789  L_24 = __this->get_end_0();
		Vector3_t3525329789  L_25 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		V_2 = L_25;
		float L_26 = Vector3_get_sqrMagnitude_m1207423764((&V_2), /*hidden argument*/NULL);
		__this->set_U3CsqrRemainingDistanceU3E__0_1(L_26);
		__this->set_U24current_4(NULL);
		__this->set_U24PC_3(1);
		goto IL_00f6;
	}

IL_00dd:
	{
		float L_27 = __this->get_U3CsqrRemainingDistanceU3E__0_1();
		if ((((float)L_27) > ((float)(1.401298E-45f))))
		{
			goto IL_004f;
		}
	}
	{
		__this->set_U24PC_3((-1));
	}

IL_00f4:
	{
		return (bool)0;
	}

IL_00f6:
	{
		return (bool)1;
	}
	// Dead block : IL_00f8: ldloc.3
}
// System.Void Completed.MovingObject/<SmoothMovement>c__Iterator0::Dispose()
extern "C"  void U3CSmoothMovementU3Ec__Iterator0_Dispose_m4078259781 (U3CSmoothMovementU3Ec__Iterator0_t791364932 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Completed.MovingObject/<SmoothMovement>c__Iterator0::Reset()
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CSmoothMovementU3Ec__Iterator0_Reset_m1029549749_MetadataUsageId;
extern "C"  void U3CSmoothMovementU3Ec__Iterator0_Reset_m1029549749 (U3CSmoothMovementU3Ec__Iterator0_t791364932 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSmoothMovementU3Ec__Iterator0_Reset_m1029549749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Completed.Player::.ctor()
extern "C"  void Player__ctor_m2611490551 (Player_t574403530 * __this, const MethodInfo* method)
{
	{
		__this->set_restartLevelDelay_7((1.0f));
		__this->set_pointsPerFood_8(((int32_t)10));
		__this->set_pointsPerSoda_9(((int32_t)20));
		__this->set_wallDamage_10(1);
		Vector2_t3525329788  L_0 = Vector2_get_one_m2767488832(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t3525329788  L_1 = Vector2_op_UnaryNegation_m1730705317(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_touchOrigin_21(L_1);
		MovingObject__ctor_m184393323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.Player::Start()
extern TypeInfo* GameManager_t550910980_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimator_t792326996_m4147395588_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2109956132;
extern const uint32_t Player_Start_m1558628343_MetadataUsageId;
extern "C"  void Player_Start_m1558628343 (Player_t574403530 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Player_Start_m1558628343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = Component_GetComponent_TisAnimator_t792326996_m4147395588(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t792326996_m4147395588_MethodInfo_var);
		__this->set_animator_19(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t550910980_il2cpp_TypeInfo_var);
		GameManager_t550910980 * L_1 = ((GameManager_t550910980_StaticFields*)GameManager_t550910980_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_playerFoodPoints_4();
		__this->set_food_20(L_2);
		Text_t3286458198 * L_3 = __this->get_foodText_11();
		int32_t L_4 = __this->get_food_20();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2109956132, L_6, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_7);
		MovingObject_Start_m3426498411(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.Player::OnDisable()
extern TypeInfo* GameManager_t550910980_il2cpp_TypeInfo_var;
extern const uint32_t Player_OnDisable_m729886686_MetadataUsageId;
extern "C"  void Player_OnDisable_m729886686 (Player_t574403530 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Player_OnDisable_m729886686_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t550910980_il2cpp_TypeInfo_var);
		GameManager_t550910980 * L_0 = ((GameManager_t550910980_StaticFields*)GameManager_t550910980_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		int32_t L_1 = __this->get_food_20();
		NullCheck(L_0);
		L_0->set_playerFoodPoints_4(L_1);
		return;
	}
}
// System.Void Completed.Player::Update()
extern TypeInfo* GameManager_t550910980_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t1593691127_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const MethodInfo* Player_AttemptMove_TisWall_t2478977715_m3215699262_MethodInfo_var;
extern const uint32_t Player_Update_m1078690550_MetadataUsageId;
extern "C"  void Player_Update_m1078690550 (Player_t574403530 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Player_Update_m1078690550_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Touch_t1603883884  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t3525329788  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t G_B11_0 = 0;
	int32_t G_B15_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t550910980_il2cpp_TypeInfo_var);
		GameManager_t550910980 * L_0 = ((GameManager_t550910980_StaticFields*)GameManager_t550910980_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		NullCheck(L_0);
		bool L_1 = L_0->get_playersTurn_6();
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		return;
	}

IL_0010:
	{
		V_0 = 0;
		V_1 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		int32_t L_2 = Input_get_touchCount_m1430909390(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_00f2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		TouchU5BU5D_t376223077* L_3 = Input_get_touches_m300368858(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		V_2 = (*(Touch_t1603883884 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		int32_t L_4 = Touch_get_phase_m3314549414((&V_2), /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_004e;
		}
	}
	{
		Vector2_t3525329788  L_5 = Touch_get_position_m1943849441((&V_2), /*hidden argument*/NULL);
		__this->set_touchOrigin_21(L_5);
		goto IL_00f2;
	}

IL_004e:
	{
		int32_t L_6 = Touch_get_phase_m3314549414((&V_2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)3))))
		{
			goto IL_00f2;
		}
	}
	{
		Vector2_t3525329788 * L_7 = __this->get_address_of_touchOrigin_21();
		float L_8 = L_7->get_x_1();
		if ((!(((float)L_8) >= ((float)(0.0f)))))
		{
			goto IL_00f2;
		}
	}
	{
		Vector2_t3525329788  L_9 = Touch_get_position_m1943849441((&V_2), /*hidden argument*/NULL);
		V_3 = L_9;
		float L_10 = (&V_3)->get_x_1();
		Vector2_t3525329788 * L_11 = __this->get_address_of_touchOrigin_21();
		float L_12 = L_11->get_x_1();
		V_4 = ((float)((float)L_10-(float)L_12));
		float L_13 = (&V_3)->get_y_2();
		Vector2_t3525329788 * L_14 = __this->get_address_of_touchOrigin_21();
		float L_15 = L_14->get_y_2();
		V_5 = ((float)((float)L_13-(float)L_15));
		Vector2_t3525329788 * L_16 = __this->get_address_of_touchOrigin_21();
		L_16->set_x_1((-1.0f));
		float L_17 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_18 = fabsf(L_17);
		float L_19 = V_5;
		float L_20 = fabsf(L_19);
		if ((!(((float)L_18) > ((float)L_20))))
		{
			goto IL_00de;
		}
	}
	{
		float L_21 = V_4;
		if ((!(((float)L_21) > ((float)(0.0f)))))
		{
			goto IL_00d7;
		}
	}
	{
		G_B11_0 = 1;
		goto IL_00d8;
	}

IL_00d7:
	{
		G_B11_0 = (-1);
	}

IL_00d8:
	{
		V_0 = G_B11_0;
		goto IL_00f2;
	}

IL_00de:
	{
		float L_22 = V_5;
		if ((!(((float)L_22) > ((float)(0.0f)))))
		{
			goto IL_00f0;
		}
	}
	{
		G_B15_0 = 1;
		goto IL_00f1;
	}

IL_00f0:
	{
		G_B15_0 = (-1);
	}

IL_00f1:
	{
		V_1 = G_B15_0;
	}

IL_00f2:
	{
		int32_t L_23 = V_0;
		if (L_23)
		{
			goto IL_00fe;
		}
	}
	{
		int32_t L_24 = V_1;
		if (!L_24)
		{
			goto IL_0106;
		}
	}

IL_00fe:
	{
		int32_t L_25 = V_0;
		int32_t L_26 = V_1;
		GenericVirtActionInvoker2< int32_t, int32_t >::Invoke(Player_AttemptMove_TisWall_t2478977715_m3215699262_MethodInfo_var, __this, L_25, L_26);
	}

IL_0106:
	{
		return;
	}
}
// System.Void Completed.Player::OnTriggerEnter2D(UnityEngine.Collider2D)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* SoundManager_t625664135_il2cpp_TypeInfo_var;
extern TypeInfo* AudioClipU5BU5D_t2889538658_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2174270;
extern Il2CppCodeGenString* _stringLiteral2762159599;
extern Il2CppCodeGenString* _stringLiteral2195582;
extern Il2CppCodeGenString* _stringLiteral43;
extern Il2CppCodeGenString* _stringLiteral445302852;
extern Il2CppCodeGenString* _stringLiteral2582521;
extern const uint32_t Player_OnTriggerEnter2D_m2789272417_MetadataUsageId;
extern "C"  void Player_OnTriggerEnter2D_m2789272417 (Player_t574403530 * __this, Collider2D_t1890038195 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Player_OnTriggerEnter2D_m2789272417_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider2D_t1890038195 * L_0 = ___other;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m217485006(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_1, _stringLiteral2174270, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		float L_3 = __this->get_restartLevelDelay_7();
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral2762159599, L_3, /*hidden argument*/NULL);
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_0167;
	}

IL_0032:
	{
		Collider2D_t1890038195 * L_4 = ___other;
		NullCheck(L_4);
		String_t* L_5 = Component_get_tag_m217485006(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_5, _stringLiteral2195582, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_00cf;
		}
	}
	{
		int32_t L_7 = __this->get_food_20();
		int32_t L_8 = __this->get_pointsPerFood_8();
		__this->set_food_20(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		Text_t3286458198 * L_9 = __this->get_foodText_11();
		ObjectU5BU5D_t11523773* L_10 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, _stringLiteral43);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral43);
		ObjectU5BU5D_t11523773* L_11 = L_10;
		int32_t L_12 = __this->get_pointsPerFood_8();
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = L_11;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		ArrayElementTypeCheck (L_15, _stringLiteral445302852);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral445302852);
		ObjectU5BU5D_t11523773* L_16 = L_15;
		int32_t L_17 = __this->get_food_20();
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 3);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_19);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m3016520001(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_9, L_20);
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t625664135_il2cpp_TypeInfo_var);
		SoundManager_t625664135 * L_21 = ((SoundManager_t625664135_StaticFields*)SoundManager_t625664135_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		AudioClipU5BU5D_t2889538658* L_22 = ((AudioClipU5BU5D_t2889538658*)SZArrayNew(AudioClipU5BU5D_t2889538658_il2cpp_TypeInfo_var, (uint32_t)2));
		AudioClip_t3714538611 * L_23 = __this->get_eatSound1_14();
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 0);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (AudioClip_t3714538611 *)L_23);
		AudioClipU5BU5D_t2889538658* L_24 = L_22;
		AudioClip_t3714538611 * L_25 = __this->get_eatSound2_15();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 1);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(1), (AudioClip_t3714538611 *)L_25);
		NullCheck(L_21);
		SoundManager_RandomizeSfx_m613386307(L_21, L_24, /*hidden argument*/NULL);
		Collider2D_t1890038195 * L_26 = ___other;
		NullCheck(L_26);
		GameObject_t4012695102 * L_27 = Component_get_gameObject_m1170635899(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m3538205401(L_27, (bool)0, /*hidden argument*/NULL);
		goto IL_0167;
	}

IL_00cf:
	{
		Collider2D_t1890038195 * L_28 = ___other;
		NullCheck(L_28);
		String_t* L_29 = Component_get_tag_m217485006(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_30 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_29, _stringLiteral2582521, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0167;
		}
	}
	{
		int32_t L_31 = __this->get_food_20();
		int32_t L_32 = __this->get_pointsPerSoda_9();
		__this->set_food_20(((int32_t)((int32_t)L_31+(int32_t)L_32)));
		Text_t3286458198 * L_33 = __this->get_foodText_11();
		ObjectU5BU5D_t11523773* L_34 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 0);
		ArrayElementTypeCheck (L_34, _stringLiteral43);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral43);
		ObjectU5BU5D_t11523773* L_35 = L_34;
		int32_t L_36 = __this->get_pointsPerSoda_9();
		int32_t L_37 = L_36;
		Il2CppObject * L_38 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_37);
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 1);
		ArrayElementTypeCheck (L_35, L_38);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_38);
		ObjectU5BU5D_t11523773* L_39 = L_35;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 2);
		ArrayElementTypeCheck (L_39, _stringLiteral445302852);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral445302852);
		ObjectU5BU5D_t11523773* L_40 = L_39;
		int32_t L_41 = __this->get_food_20();
		int32_t L_42 = L_41;
		Il2CppObject * L_43 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 3);
		ArrayElementTypeCheck (L_40, L_43);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_43);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m3016520001(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		NullCheck(L_33);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_33, L_44);
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t625664135_il2cpp_TypeInfo_var);
		SoundManager_t625664135 * L_45 = ((SoundManager_t625664135_StaticFields*)SoundManager_t625664135_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		AudioClipU5BU5D_t2889538658* L_46 = ((AudioClipU5BU5D_t2889538658*)SZArrayNew(AudioClipU5BU5D_t2889538658_il2cpp_TypeInfo_var, (uint32_t)2));
		AudioClip_t3714538611 * L_47 = __this->get_drinkSound1_16();
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, 0);
		ArrayElementTypeCheck (L_46, L_47);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(0), (AudioClip_t3714538611 *)L_47);
		AudioClipU5BU5D_t2889538658* L_48 = L_46;
		AudioClip_t3714538611 * L_49 = __this->get_drinkSound2_17();
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 1);
		ArrayElementTypeCheck (L_48, L_49);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(1), (AudioClip_t3714538611 *)L_49);
		NullCheck(L_45);
		SoundManager_RandomizeSfx_m613386307(L_45, L_48, /*hidden argument*/NULL);
		Collider2D_t1890038195 * L_50 = ___other;
		NullCheck(L_50);
		GameObject_t4012695102 * L_51 = Component_get_gameObject_m1170635899(L_50, /*hidden argument*/NULL);
		NullCheck(L_51);
		GameObject_SetActive_m3538205401(L_51, (bool)0, /*hidden argument*/NULL);
	}

IL_0167:
	{
		return;
	}
}
// System.Void Completed.Player::Restart()
extern "C"  void Player_Restart_m3465508196 (Player_t574403530 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_loadedLevel_m946446301(NULL /*static, unused*/, /*hidden argument*/NULL);
		Application_LoadLevel_m1181471414(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.Player::LoseFood(System.Int32)
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2422900978;
extern Il2CppCodeGenString* _stringLiteral45;
extern Il2CppCodeGenString* _stringLiteral445302852;
extern const uint32_t Player_LoseFood_m3579051793_MetadataUsageId;
extern "C"  void Player_LoseFood_m3579051793 (Player_t574403530 * __this, int32_t ___loss, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Player_LoseFood_m3579051793_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_animator_19();
		NullCheck(L_0);
		Animator_SetTrigger_m514363822(L_0, _stringLiteral2422900978, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_food_20();
		int32_t L_2 = ___loss;
		__this->set_food_20(((int32_t)((int32_t)L_1-(int32_t)L_2)));
		Text_t3286458198 * L_3 = __this->get_foodText_11();
		ObjectU5BU5D_t11523773* L_4 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, _stringLiteral45);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral45);
		ObjectU5BU5D_t11523773* L_5 = L_4;
		int32_t L_6 = ___loss;
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_8);
		ObjectU5BU5D_t11523773* L_9 = L_5;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 2);
		ArrayElementTypeCheck (L_9, _stringLiteral445302852);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral445302852);
		ObjectU5BU5D_t11523773* L_10 = L_9;
		int32_t L_11 = __this->get_food_20();
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_14);
		Player_CheckIfGameOver_m2655384480(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.Player::CheckIfGameOver()
extern TypeInfo* SoundManager_t625664135_il2cpp_TypeInfo_var;
extern TypeInfo* GameManager_t550910980_il2cpp_TypeInfo_var;
extern const uint32_t Player_CheckIfGameOver_m2655384480_MetadataUsageId;
extern "C"  void Player_CheckIfGameOver_m2655384480 (Player_t574403530 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Player_CheckIfGameOver_m2655384480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_food_20();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t625664135_il2cpp_TypeInfo_var);
		SoundManager_t625664135 * L_1 = ((SoundManager_t625664135_StaticFields*)SoundManager_t625664135_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		AudioClip_t3714538611 * L_2 = __this->get_gameOverSound_18();
		NullCheck(L_1);
		SoundManager_PlaySingle_m348720029(L_1, L_2, /*hidden argument*/NULL);
		SoundManager_t625664135 * L_3 = ((SoundManager_t625664135_StaticFields*)SoundManager_t625664135_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_3);
		AudioSource_t3628549054 * L_4 = L_3->get_musicSource_3();
		NullCheck(L_4);
		AudioSource_Stop_m1454243038(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t550910980_il2cpp_TypeInfo_var);
		GameManager_t550910980 * L_5 = ((GameManager_t550910980_StaticFields*)GameManager_t550910980_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		NullCheck(L_5);
		GameManager_GameOver_m3163353303(L_5, /*hidden argument*/NULL);
	}

IL_0035:
	{
		return;
	}
}
// System.Void Completed.SoundManager::.ctor()
extern "C"  void SoundManager__ctor_m639677210 (SoundManager_t625664135 * __this, const MethodInfo* method)
{
	{
		__this->set_lowPitchRange_5((0.95f));
		__this->set_highPitchRange_6((1.05f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.SoundManager::.cctor()
extern "C"  void SoundManager__cctor_m2168028115 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Completed.SoundManager::Awake()
extern TypeInfo* SoundManager_t625664135_il2cpp_TypeInfo_var;
extern const uint32_t SoundManager_Awake_m877282429_MetadataUsageId;
extern "C"  void SoundManager_Awake_m877282429 (SoundManager_t625664135 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SoundManager_Awake_m877282429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t625664135_il2cpp_TypeInfo_var);
		SoundManager_t625664135 * L_0 = ((SoundManager_t625664135_StaticFields*)SoundManager_t625664135_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t625664135_il2cpp_TypeInfo_var);
		((SoundManager_t625664135_StaticFields*)SoundManager_t625664135_il2cpp_TypeInfo_var->static_fields)->set_instance_4(__this);
		goto IL_0036;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t625664135_il2cpp_TypeInfo_var);
		SoundManager_t625664135 * L_2 = ((SoundManager_t625664135_StaticFields*)SoundManager_t625664135_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, __this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0036:
	{
		GameObject_t4012695102 * L_5 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m4064482788(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.SoundManager::PlaySingle(UnityEngine.AudioClip)
extern "C"  void SoundManager_PlaySingle_m348720029 (SoundManager_t625664135 * __this, AudioClip_t3714538611 * ___clip, const MethodInfo* method)
{
	{
		AudioSource_t3628549054 * L_0 = __this->get_efxSource_2();
		AudioClip_t3714538611 * L_1 = ___clip;
		NullCheck(L_0);
		AudioSource_set_clip_m19502010(L_0, L_1, /*hidden argument*/NULL);
		AudioSource_t3628549054 * L_2 = __this->get_efxSource_2();
		NullCheck(L_2);
		AudioSource_Play_m1360558992(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.SoundManager::RandomizeSfx(UnityEngine.AudioClip[])
extern "C"  void SoundManager_RandomizeSfx_m613386307 (SoundManager_t625664135 * __this, AudioClipU5BU5D_t2889538658* ___clips, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		AudioClipU5BU5D_t2889538658* L_0 = ___clips;
		NullCheck(L_0);
		int32_t L_1 = Random_Range_m75452833(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))), /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = __this->get_lowPitchRange_5();
		float L_3 = __this->get_highPitchRange_6();
		float L_4 = Random_Range_m3362417303(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		AudioSource_t3628549054 * L_5 = __this->get_efxSource_2();
		float L_6 = V_1;
		NullCheck(L_5);
		AudioSource_set_pitch_m1518407234(L_5, L_6, /*hidden argument*/NULL);
		AudioSource_t3628549054 * L_7 = __this->get_efxSource_2();
		AudioClipU5BU5D_t2889538658* L_8 = ___clips;
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		NullCheck(L_7);
		AudioSource_set_clip_m19502010(L_7, ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), /*hidden argument*/NULL);
		AudioSource_t3628549054 * L_11 = __this->get_efxSource_2();
		NullCheck(L_11);
		AudioSource_Play_m1360558992(L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.Wall::.ctor()
extern "C"  void Wall__ctor_m1036875630 (Wall_t2478977715 * __this, const MethodInfo* method)
{
	{
		__this->set_hp_5(3);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Completed.Wall::Awake()
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t2223784725_m4090179846_MethodInfo_var;
extern const uint32_t Wall_Awake_m1274480849_MetadataUsageId;
extern "C"  void Wall_Awake_m1274480849 (Wall_t2478977715 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Wall_Awake_m1274480849_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SpriteRenderer_t2223784725 * L_0 = Component_GetComponent_TisSpriteRenderer_t2223784725_m4090179846(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t2223784725_m4090179846_MethodInfo_var);
		__this->set_spriteRenderer_6(L_0);
		return;
	}
}
// System.Void Completed.Wall::DamageWall(System.Int32)
extern TypeInfo* SoundManager_t625664135_il2cpp_TypeInfo_var;
extern TypeInfo* AudioClipU5BU5D_t2889538658_il2cpp_TypeInfo_var;
extern const uint32_t Wall_DamageWall_m148677312_MetadataUsageId;
extern "C"  void Wall_DamageWall_m148677312 (Wall_t2478977715 * __this, int32_t ___loss, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Wall_DamageWall_m148677312_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t625664135_il2cpp_TypeInfo_var);
		SoundManager_t625664135 * L_0 = ((SoundManager_t625664135_StaticFields*)SoundManager_t625664135_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		AudioClipU5BU5D_t2889538658* L_1 = ((AudioClipU5BU5D_t2889538658*)SZArrayNew(AudioClipU5BU5D_t2889538658_il2cpp_TypeInfo_var, (uint32_t)2));
		AudioClip_t3714538611 * L_2 = __this->get_chopSound1_2();
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (AudioClip_t3714538611 *)L_2);
		AudioClipU5BU5D_t2889538658* L_3 = L_1;
		AudioClip_t3714538611 * L_4 = __this->get_chopSound2_3();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (AudioClip_t3714538611 *)L_4);
		NullCheck(L_0);
		SoundManager_RandomizeSfx_m613386307(L_0, L_3, /*hidden argument*/NULL);
		SpriteRenderer_t2223784725 * L_5 = __this->get_spriteRenderer_6();
		Sprite_t4006040370 * L_6 = __this->get_dmgSprite_4();
		NullCheck(L_5);
		SpriteRenderer_set_sprite_m1519408453(L_5, L_6, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_hp_5();
		int32_t L_8 = ___loss;
		__this->set_hp_5(((int32_t)((int32_t)L_7-(int32_t)L_8)));
		int32_t L_9 = __this->get_hp_5();
		if ((((int32_t)L_9) > ((int32_t)0)))
		{
			goto IL_0059;
		}
	}
	{
		GameObject_t4012695102 * L_10 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_SetActive_m3538205401(L_10, (bool)0, /*hidden argument*/NULL);
	}

IL_0059:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
