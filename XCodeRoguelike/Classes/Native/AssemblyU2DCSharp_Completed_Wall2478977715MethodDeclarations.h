﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Completed.Wall
struct Wall_t2478977715;

#include "codegen/il2cpp-codegen.h"

// System.Void Completed.Wall::.ctor()
extern "C"  void Wall__ctor_m1036875630 (Wall_t2478977715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.Wall::Awake()
extern "C"  void Wall_Awake_m1274480849 (Wall_t2478977715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.Wall::DamageWall(System.Int32)
extern "C"  void Wall_DamageWall_m148677312 (Wall_t2478977715 * __this, int32_t ___loss, const MethodInfo* method) IL2CPP_METHOD_ATTR;
