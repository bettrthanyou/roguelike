﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Completed.Loader
struct  Loader_t462637308  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.GameObject Completed.Loader::gameManager
	GameObject_t4012695102 * ___gameManager_2;
	// UnityEngine.GameObject Completed.Loader::soundManager
	GameObject_t4012695102 * ___soundManager_3;

public:
	inline static int32_t get_offset_of_gameManager_2() { return static_cast<int32_t>(offsetof(Loader_t462637308, ___gameManager_2)); }
	inline GameObject_t4012695102 * get_gameManager_2() const { return ___gameManager_2; }
	inline GameObject_t4012695102 ** get_address_of_gameManager_2() { return &___gameManager_2; }
	inline void set_gameManager_2(GameObject_t4012695102 * value)
	{
		___gameManager_2 = value;
		Il2CppCodeGenWriteBarrier(&___gameManager_2, value);
	}

	inline static int32_t get_offset_of_soundManager_3() { return static_cast<int32_t>(offsetof(Loader_t462637308, ___soundManager_3)); }
	inline GameObject_t4012695102 * get_soundManager_3() const { return ___soundManager_3; }
	inline GameObject_t4012695102 ** get_address_of_soundManager_3() { return &___soundManager_3; }
	inline void set_soundManager_3(GameObject_t4012695102 * value)
	{
		___soundManager_3 = value;
		Il2CppCodeGenWriteBarrier(&___soundManager_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
