﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1408070318MethodDeclarations.h"

// System.Void System.Predicate`1<Completed.Enemy>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m4160641018(__this, ___object, ___method, method) ((  void (*) (Predicate_1_t3114353643 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m982040097_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Completed.Enemy>::Invoke(T)
#define Predicate_1_Invoke_m4074472076(__this, ___obj, method) ((  bool (*) (Predicate_1_t3114353643 *, Enemy_t2543389745 *, const MethodInfo*))Predicate_1_Invoke_m4106178309_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Completed.Enemy>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m36425567(__this, ___obj, ___callback, ___object, method) ((  Il2CppObject * (*) (Predicate_1_t3114353643 *, Enemy_t2543389745 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Completed.Enemy>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m2898268360(__this, ___result, method) ((  bool (*) (Predicate_1_t3114353643 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result, method)
