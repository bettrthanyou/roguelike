﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Completed.Enemy
struct Enemy_t2543389745;

#include "codegen/il2cpp-codegen.h"

// System.Void Completed.Enemy::.ctor()
extern "C"  void Enemy__ctor_m36979494 (Enemy_t2543389745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.Enemy::Start()
extern "C"  void Enemy_Start_m3279084582 (Enemy_t2543389745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.Enemy::MoveEnemy()
extern "C"  void Enemy_MoveEnemy_m1036909371 (Enemy_t2543389745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
