﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Completed.GameManager/<MoveEnemies>c__Iterator1
struct U3CMoveEnemiesU3Ec__Iterator1_t3054131685;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Completed.GameManager/<MoveEnemies>c__Iterator1::.ctor()
extern "C"  void U3CMoveEnemiesU3Ec__Iterator1__ctor_m2976783517 (U3CMoveEnemiesU3Ec__Iterator1_t3054131685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Completed.GameManager/<MoveEnemies>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMoveEnemiesU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1849018581 (U3CMoveEnemiesU3Ec__Iterator1_t3054131685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Completed.GameManager/<MoveEnemies>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMoveEnemiesU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3296073321 (U3CMoveEnemiesU3Ec__Iterator1_t3054131685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Completed.GameManager/<MoveEnemies>c__Iterator1::MoveNext()
extern "C"  bool U3CMoveEnemiesU3Ec__Iterator1_MoveNext_m3174090807 (U3CMoveEnemiesU3Ec__Iterator1_t3054131685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.GameManager/<MoveEnemies>c__Iterator1::Dispose()
extern "C"  void U3CMoveEnemiesU3Ec__Iterator1_Dispose_m139023770 (U3CMoveEnemiesU3Ec__Iterator1_t3054131685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.GameManager/<MoveEnemies>c__Iterator1::Reset()
extern "C"  void U3CMoveEnemiesU3Ec__Iterator1_Reset_m623216458 (U3CMoveEnemiesU3Ec__Iterator1_t3054131685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
