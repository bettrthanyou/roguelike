﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Completed.SoundManager
struct SoundManager_t625664135;
// UnityEngine.AudioClip
struct AudioClip_t3714538611;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t2889538658;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AudioClip3714538611.h"

// System.Void Completed.SoundManager::.ctor()
extern "C"  void SoundManager__ctor_m639677210 (SoundManager_t625664135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.SoundManager::.cctor()
extern "C"  void SoundManager__cctor_m2168028115 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.SoundManager::Awake()
extern "C"  void SoundManager_Awake_m877282429 (SoundManager_t625664135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.SoundManager::PlaySingle(UnityEngine.AudioClip)
extern "C"  void SoundManager_PlaySingle_m348720029 (SoundManager_t625664135 * __this, AudioClip_t3714538611 * ___clip, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Completed.SoundManager::RandomizeSfx(UnityEngine.AudioClip[])
extern "C"  void SoundManager_RandomizeSfx_m613386307 (SoundManager_t625664135 * __this, AudioClipU5BU5D_t2889538658* ___clips, const MethodInfo* method) IL2CPP_METHOD_ATTR;
