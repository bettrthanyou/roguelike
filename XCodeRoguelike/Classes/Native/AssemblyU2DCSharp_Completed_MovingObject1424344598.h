﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.BoxCollider2D
struct BoxCollider2D_t262790558;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t3632243084;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_LayerMask1862190090.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Completed.MovingObject
struct  MovingObject_t1424344598  : public MonoBehaviour_t3012272455
{
public:
	// System.Single Completed.MovingObject::moveTime
	float ___moveTime_2;
	// UnityEngine.LayerMask Completed.MovingObject::blockingLayer
	LayerMask_t1862190090  ___blockingLayer_3;
	// UnityEngine.BoxCollider2D Completed.MovingObject::boxCollider
	BoxCollider2D_t262790558 * ___boxCollider_4;
	// UnityEngine.Rigidbody2D Completed.MovingObject::rb2D
	Rigidbody2D_t3632243084 * ___rb2D_5;
	// System.Single Completed.MovingObject::inverseMoveTime
	float ___inverseMoveTime_6;

public:
	inline static int32_t get_offset_of_moveTime_2() { return static_cast<int32_t>(offsetof(MovingObject_t1424344598, ___moveTime_2)); }
	inline float get_moveTime_2() const { return ___moveTime_2; }
	inline float* get_address_of_moveTime_2() { return &___moveTime_2; }
	inline void set_moveTime_2(float value)
	{
		___moveTime_2 = value;
	}

	inline static int32_t get_offset_of_blockingLayer_3() { return static_cast<int32_t>(offsetof(MovingObject_t1424344598, ___blockingLayer_3)); }
	inline LayerMask_t1862190090  get_blockingLayer_3() const { return ___blockingLayer_3; }
	inline LayerMask_t1862190090 * get_address_of_blockingLayer_3() { return &___blockingLayer_3; }
	inline void set_blockingLayer_3(LayerMask_t1862190090  value)
	{
		___blockingLayer_3 = value;
	}

	inline static int32_t get_offset_of_boxCollider_4() { return static_cast<int32_t>(offsetof(MovingObject_t1424344598, ___boxCollider_4)); }
	inline BoxCollider2D_t262790558 * get_boxCollider_4() const { return ___boxCollider_4; }
	inline BoxCollider2D_t262790558 ** get_address_of_boxCollider_4() { return &___boxCollider_4; }
	inline void set_boxCollider_4(BoxCollider2D_t262790558 * value)
	{
		___boxCollider_4 = value;
		Il2CppCodeGenWriteBarrier(&___boxCollider_4, value);
	}

	inline static int32_t get_offset_of_rb2D_5() { return static_cast<int32_t>(offsetof(MovingObject_t1424344598, ___rb2D_5)); }
	inline Rigidbody2D_t3632243084 * get_rb2D_5() const { return ___rb2D_5; }
	inline Rigidbody2D_t3632243084 ** get_address_of_rb2D_5() { return &___rb2D_5; }
	inline void set_rb2D_5(Rigidbody2D_t3632243084 * value)
	{
		___rb2D_5 = value;
		Il2CppCodeGenWriteBarrier(&___rb2D_5, value);
	}

	inline static int32_t get_offset_of_inverseMoveTime_6() { return static_cast<int32_t>(offsetof(MovingObject_t1424344598, ___inverseMoveTime_6)); }
	inline float get_inverseMoveTime_6() const { return ___inverseMoveTime_6; }
	inline float* get_address_of_inverseMoveTime_6() { return &___inverseMoveTime_6; }
	inline void set_inverseMoveTime_6(float value)
	{
		___inverseMoveTime_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
