﻿using UnityEngine;
using System;
using System.Collections.Generic; 		//Allows us to use Lists.
using Random = UnityEngine.Random; 		//Tells Random to use the Unity Engine random number generator.

namespace Completed
	
{
	
	public class BoardManager : MonoBehaviour
	{
		// Using Serializable allows us to embed a class with sub properties in the inspector.
		[Serializable]
		public class Count
		{
			public int minimum; 			//Minimum value for our Count class.
			public int maximum; 			//Maximum value for our Count class.
			
			
			//Assignment constructor.
			public Count (int min, int max)
			{
				minimum = min;
				maximum = max;
			}
		}
		
		
		public int columns = 80; 										//Number of columns in our game board.
		public int rows = 80;											//Number of rows in our game board.
																		//Lower and upper limit for our random number of walls per level.
		public Count foodCount = new Count (0, 1);						//Lower and upper limit for our random number of food items per level.

		public GameObject exit;											//Prefab to spawn for exit.
		public GameObject[] floorTiles;									//Array of floor prefabs.
		public GameObject[] wallTiles;									//Array of wall prefabs.
		public GameObject[] foodTiles;									//Array of food prefabs.
		public GameObject[] enemyTiles;									//Array of enemy prefabs.
		public GameObject[] outerWallTiles;								//Array of outer tile prefabs.
		
		private Transform boardHolder;									//A variable to store a reference to the transform of our Board object.
		private List <Vector3> gridPositions = new List <Vector3> ();	//A list of possible locations to place tiles.
		private List <Vector3> emptyPositions = new List <Vector3> ();
		
		//Clears our list gridPositions and prepares it to generate a new board.
		void InitialiseList ()
		{
			//Clear our list gridPositions.
			gridPositions.Clear ();
			
			//Loop through x axis (columns).
			for(int x = 0; x < columns; x++)
			{
				//Within each column, loop through y axis (rows).
				for(int y = 0; y < rows; y++)
				{
					//At each index add a new Vector3 to our list with the x and y coordinates of that position.
						gridPositions.Add (new Vector3(x, y, 0f));
				}
			}
		}
		
		
		//Sets up the outer walls and floor (background) of the game board.
		void BoardSetup ()
		{
			//Instantiate Board and set boardHolder to its transform.
			boardHolder = new GameObject ("Board").transform;
			
			//Loop along x axis, starting from -1 (to fill corner) with floor or outerwall edge tiles.
			for(int x = -6; x < columns + 6; x++)
			{
				//Loop along y axis, starting from -1 to place floor or outerwall tiles.
				for(int y = -6; y < rows + 6; y++)
				{
					//Choose a random tile from our array of floor tile prefabs and prepare to instantiate it.
					GameObject toInstantiate = floorTiles[Random.Range (0,floorTiles.Length)];
					
					//Check if we current position is at board edge, if so choose a random outer wall prefab from our array of outer wall tiles.
					if(x <= -1 || x >= columns || y <= -1 || y >= rows)
						toInstantiate = outerWallTiles [Random.Range (0, outerWallTiles.Length)];
					
					//Instantiate the GameObject instance using the prefab chosen for toInstantiate at the Vector3 corresponding to current grid position in loop, cast it to GameObject.
					GameObject instance =
						Instantiate (toInstantiate, new Vector3 (x, y, 0f), Quaternion.identity) as GameObject;
					
					//Set the parent of our newly instantiated object instance to boardHolder, this is just organizational to avoid cluttering hierarchy.
					instance.transform.SetParent (boardHolder);
				}
			}
		}
		
		
		//RandomPosition returns a random position from our list gridPositions.
		Vector3 RandomPosition ()
		{
			//Declare an integer randomIndex, set it's value to a random number between 0 and the count of items in our List gridPositions.
			int randomIndex = Random.Range (0, emptyPositions.Count);
			
			//Declare a variable of type Vector3 called randomPosition, set it's value to the entry at randomIndex from our List gridPositions.
			Vector3 randomPosition = emptyPositions[randomIndex];
			
			//Remove the entry at randomIndex from the list so that it can't be re-used.
			emptyPositions.RemoveAt (randomIndex);
			
			//Return the randomly selected Vector3 position.
			return randomPosition;
		}
		
		
		//LayoutObjectAtRandom accepts an array of game objects to choose from along with a minimum and maximum range for the number of objects to create.
		void LayoutObjectAtRandom (GameObject[] tileArray, int minimum, int maximum)
		{


			//Choose a random number of objects to instantiate within the minimum and maximum limits
			int objectCount = Random.Range (minimum, maximum+1);
			
			//Instantiate objects until the randomly chosen limit objectCount is reached
			for(int i = 0; i < objectCount; i++)
			{
				//Choose a position for randomPosition by getting a random position from our list of available Vector3s stored in gridPosition
				Vector3 randomPosition = RandomPosition();
				
				//Choose a random tile from tileArray and assign it to tileChoice
				GameObject tileChoice = tileArray[Random.Range (0, tileArray.Length)];
				
				//Instantiate tileChoice at the position returned by RandomPosition with no change in rotation
				Instantiate(tileChoice, randomPosition, Quaternion.identity);
			}
		}

		void LayoutInnerWalls (GameObject[] tileArray)
		{	
			
			print ("swag");
			for (int i = 1; i < gridPositions.Count-1; i++) {
				GameObject tileChoice = tileArray [Random.Range (0, tileArray.Length)];
				Instantiate (tileChoice, gridPositions [i], Quaternion.identity);
			}



		}
		//chance = 2 for equal, up = false for right-leaning, true for up-leaning
		void LayoutPath (int chance, bool up){
			print ("A");
			int pos = 0;
			int lastRoom = rows - 1;
			Vector3 n = new Vector3 (-20.0f, -20.0f, 0f);
			while (true) {
				gridPositions.Insert (pos, n);
				emptyPositions.Add (gridPositions [pos + 1]);
				gridPositions.Remove (gridPositions [pos+1]);

				if (Random.Range (lastRoom, rows) == rows - 1) {
					//int x = Random.Range (1, rows);
					int x = 8;
					x /= 4;
					int ybot = x;
					int ytop = x;
					print (x);
					if (ybot > pos % rows)
						ybot = pos % rows;
					if (ytop > rows - pos % rows - 1)
						ytop = rows - pos % rows - 1;
					for (int ecks = (-x * rows); ecks <= (x * rows); ecks += rows) {
						for (int why = -ybot; why <= ytop; why++) {
							if (pos + ecks + why > -1 && pos + ecks + why < rows * columns) {
								gridPositions.Insert (pos + ecks + why, n);
								emptyPositions.Add (gridPositions [pos + ecks + why + 1]);
								gridPositions.Remove (gridPositions [pos + ecks + why + 1]);
							}
						}
					}
					lastRoom = 0;
				} else
					lastRoom++;
				
				if (pos == columns * rows - 1) {
					print ("break");
					break;
				}
				if (pos % rows == rows - 1) {
					print ("Force Right");
					pos += rows;
				} else if (pos >= (rows * columns) - rows) {
					print ("Force Up");
					pos += 1;
				} else {
					if (up) {
						if (Random.Range (0, chance) == 0) {
							pos += 1;
							print ("Random Up");
						} else {
							pos += rows;
							print ("Random Right");
						}
					} else {
						if (Random.Range (0, chance) == 0) {
							pos += rows;
							print ("Random Right");
						} else {
							pos += 1;
							print ("Random Up");
						}
					}
				}
			}
			print ("Z");
		}

		//SetupScene initializes our level and calls the previous functions to lay out the game board
		public void SetupScene (int level)
		{
			columns = Random.Range (columns + 1, columns + 3);
			rows = Random.Range (rows + 1, rows + 3);
			//Creates the outer walls and floor.
			BoardSetup ();
			
			//Reset our list of gridpositions.
			InitialiseList ();
			
			//Instantiate a random number of wall tiles based on minimum and maximum, at randomized positions.
			//LayoutObjectAtRandom (wallTiles, gridPositions.Count, gridPositions.Count);
			LayoutPath (2, false);
			LayoutPath (4, true);
			LayoutPath (4, false);
			LayoutInnerWalls(wallTiles);

			InitialiseList ();
			emptyPositions.Remove (emptyPositions [0]);
			//Instantiate a random number of food tiles based on minimum and maximum, at randomized positions.
			LayoutObjectAtRandom (foodTiles, foodCount.minimum, foodCount.maximum);
			

			int enemyCount = Random.Range ((int) (Math.Sqrt(columns*rows)-1), (int) (Math.Sqrt(columns*rows)+2));
			
			//Instantiate a random number of enemies based on minimum and maximum, at randomized positions.
			LayoutObjectAtRandom (enemyTiles, enemyCount, enemyCount);
			
			//Instantiate the exit tile in the upper right hand corner of our game board
			Instantiate (exit, new Vector3 (columns - 1, rows - 1, 0f), Quaternion.identity);
		}
	}
}
